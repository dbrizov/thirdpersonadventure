﻿namespace ThirdPersonAdventure
{
    public interface IHealthInterface
    {
        int GetHealth();
        int GetMaxHealth();
    }
}
