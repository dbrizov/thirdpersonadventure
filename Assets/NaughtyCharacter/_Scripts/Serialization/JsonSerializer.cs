﻿using Newtonsoft.Json;

namespace ThirdPersonAdventure
{
    public static class JsonSerializer
    {
        public static string Serialize(object obj, bool compressedJson = false)
        {
            Formatting formatting = compressedJson ? Formatting.None : Formatting.Indented;
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.Auto
            };

            return JsonConvert.SerializeObject(obj, formatting, settings);
        }

        public static T Deserialize<T>(string json)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.Auto
            };

            return JsonConvert.DeserializeObject<T>(json, settings);
        }
    }
}
