﻿using System;
using System.Collections.Generic;

namespace ThirdPersonAdventure
{
    public class LevelSaveData
    {
        public string SceneName { get; set; }
        public Dictionary<string, ObjectSaveData> Objects { get; set; }

        public LevelSaveData()
        {
            SceneName = null;
            Objects = new Dictionary<string, ObjectSaveData>();
        }

        public LevelSaveData(string sceneName, SerializableMonoBehaviour[] serializables)
        {
            SceneName = sceneName;
            Objects = new Dictionary<string, ObjectSaveData>();
            foreach (var serializable in serializables)
            {
                Objects.Add(serializable.Id, serializable.GetSaveData());
            }
        }
    }
}
