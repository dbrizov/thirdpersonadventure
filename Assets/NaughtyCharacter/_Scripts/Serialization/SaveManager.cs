﻿using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ThirdPersonAdventure
{
    public class SaveManager : MonoBehaviour
    {
        public static LevelSaveData LevelSaveData { get; set; }

        private void Start()
        {
            if (LevelSaveData != null)
            {
                Load(LevelSaveData);
                LevelSaveData = null;
            }
        }

        [Button(enabledMode: EButtonEnableMode.Playmode)]
        public static void QuickSave()
        {
            Save("QuickSave");
        }

        [Button(enabledMode: EButtonEnableMode.Playmode)]
        public static void QuickLoad()
        {
            Load("QuickSave");
        }

        [Button(enabledMode: EButtonEnableMode.Playmode)]
        public static void ManualSave()
        {
            Save("ManualSave");
        }

        [Button(enabledMode: EButtonEnableMode.Playmode)]
        public static void ManualLoad()
        {
            Load("ManualSave");
        }

        public static LevelSaveData GetLevelSaveData(string saveName)
        {
            string json = IOUtils.ReadFromFile($"{IOUtils.GetPersistentDataPath()}/{saveName}.json");
            LevelSaveData levelSaveData = JsonSerializer.Deserialize<LevelSaveData>(json);

            return levelSaveData;
        }

        public static void Save(string saveName)
        {
            string sceneName = SceneManager.GetActiveScene().name;
            var serializables = FindObjectsOfType<SerializableMonoBehaviour>();
            LevelSaveData levelSaveData = new LevelSaveData(sceneName, serializables);
            string json = JsonSerializer.Serialize(levelSaveData);
            string filePath = $"{IOUtils.GetPersistentDataPath()}/{saveName}.json";
            IOUtils.WriteToFile(filePath, json);
        }

        public static void Load(string saveName)
        {
            LevelSaveData levelSaveData = GetLevelSaveData(saveName);
            Load(levelSaveData);
        }

        public static void Load(LevelSaveData levelSaveData)
        {
            var serializables = FindObjectsOfType<SerializableMonoBehaviour>();
            foreach (var serializable in serializables)
            {
                if (levelSaveData.Objects.TryGetValue(serializable.Id, out ObjectSaveData data))
                {
                    serializable.ApplySaveData(data);
                }
            }
        }
    }
}
