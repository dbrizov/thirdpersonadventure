﻿using NaughtyAttributes;
using System;
using UnityEngine;

namespace ThirdPersonAdventure
{
    public class SerializableMonoBehaviour : MonoBehaviour
    {
        [Header("Serialization")]
        [ReadOnly]
        public string Id;

        private void OnValidate()
        {
            if (string.IsNullOrEmpty(Id))
            {
                Id = Guid.NewGuid().ToString();
            }
        }

        public virtual void ApplySaveData(ObjectSaveData data)
        {
            transform.position = data.Position;
            transform.rotation = Quaternion.Euler(data.EulerRotation);
        }

        public virtual ObjectSaveData GetSaveData()
        {
            ObjectSaveData data = new ObjectSaveData();
            data.Position = transform.position;
            data.EulerRotation = transform.rotation.eulerAngles;

            return data;
        }
    }
}