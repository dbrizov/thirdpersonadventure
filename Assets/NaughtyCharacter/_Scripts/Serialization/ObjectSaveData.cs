﻿using UnityEngine;

namespace ThirdPersonAdventure
{
    public class ObjectSaveData
    {
        public Vector3 Position { get; set; }
        public Vector3 EulerRotation { get; set; }

        public ObjectSaveData() { }

        public ObjectSaveData(ObjectSaveData data)
        {
            Position = data.Position;
            EulerRotation = data.EulerRotation;
        }
    }
}
