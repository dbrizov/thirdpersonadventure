﻿namespace ThirdPersonAdventure
{
    public class CharacterSaveData : ObjectSaveData
    {
        public int Health { get; set; }
        public int MaxHealth { get; set; }

        public CharacterSaveData() { }

        public CharacterSaveData(ObjectSaveData data) : base(data) { }

        public CharacterSaveData(CharacterSaveData data) : base(data)
        {
            Health = data.Health;
            MaxHealth = data.MaxHealth;
        }
    }
}
