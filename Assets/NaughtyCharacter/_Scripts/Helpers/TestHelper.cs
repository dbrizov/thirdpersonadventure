using UnityEngine;

namespace NaughtyCharacter
{
	public class TestHelper : MonoBehaviour
	{
		[SerializeField]
		private int _targetFrameRate = 60;

		[SerializeField]
		private float _slowMotion = 0.1f;

		private void Awake()
		{
			Application.targetFrameRate = _targetFrameRate;
		}

        private void Update()
        {
			if (Input.GetButtonDown("Slow Motion")) {
				Time.timeScale = Time.timeScale == 1f ? _slowMotion : 1f;
			}
        }
	}
}
