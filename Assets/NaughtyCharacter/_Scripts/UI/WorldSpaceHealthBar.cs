﻿using UnityEngine;
using UnityEngine.UI;

namespace ThirdPersonAdventure
{
    public class WorldSpaceHealthBar : MonoBehaviour
    {
        public Image FillImage;
        public GameObject HealthInterfaceObject;

        private CanvasGroup _canvasGroup;
        private Camera _mainCamera;
        private IHealthInterface _healthInterface;

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            _mainCamera = Camera.main;
            _healthInterface = HealthInterfaceObject.GetComponent<IHealthInterface>();
        }

        private void Update()
        {
            int health = _healthInterface.GetHealth();
            int maxHealth = _healthInterface.GetMaxHealth();

            if (health < maxHealth && health != 0)
            {
                _canvasGroup.alpha = 1;
            }
            else
            {
                _canvasGroup.alpha = 0;
            }

            FillImage.fillAmount = (float)health / maxHealth;

            Vector3 lookDirection = transform.position - _mainCamera.transform.position;
            transform.rotation = Quaternion.LookRotation(lookDirection, Vector3.up);
        }
    }
}
