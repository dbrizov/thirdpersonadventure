using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ThirdPersonAdventure
{
    public class InGameUIManager : MonoBehaviour
    {
        public GameObject PauseMenu;
        public Button ResumeButton;
        public Button QuitButton;
        public Image HealthBarFillImage;

        private void Awake()
        {
            PauseMenu.SetActive(false);
        }

        private void OnEnable()
        {
            ResumeButton.onClick.AddListener(() => SetPaused(false));
            QuitButton.onClick.AddListener(() =>
            {
                SetPaused(false);
                SceneManager.LoadScene("MainMenu");
            });
        }

        private void OnDisable()
        {
            ResumeButton.onClick.RemoveAllListeners();
            QuitButton.onClick.RemoveAllListeners();
        }

        private void Update()
        {
            Character playerCharacter = World.GetPlayerCharacter();
            HealthBarFillImage.fillAmount = (float)playerCharacter.Health / playerCharacter.MaxHealth;

            if (Input.GetButtonDown("Pause"))
            {
                TogglePaused();
            }
        }

        private void SetPaused(bool paused)
        {
            Time.timeScale = paused ? 0f : 1f;
            PauseMenu.SetActive(paused);
        }

        private void TogglePaused()
        {
            SetPaused(!PauseMenu.activeSelf);
        }
    }
}
