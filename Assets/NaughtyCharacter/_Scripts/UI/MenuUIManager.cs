﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ThirdPersonAdventure
{
    public class MenuUIManager : MonoBehaviour
    {
        [Header("Main Screen")]
        public GameObject MainScreen;
        public Button StartGameButton;
        public Button OptionsButton;
        public Button LoadManualSaveButton;
        public Button ExitButton;

        [Header("Options Screen")]
        public GameObject OptionsScreen;
        public Button BackButton;

        [Header("Loading Screen")]
        public GameObject LoadingScreen;
        public Image LoadingFillImage;

        private List<GameObject> _screens;

        private void Awake()
        {
            _screens = new List<GameObject>()
            {
                MainScreen,
                OptionsScreen,
                LoadingScreen
            };

            OpenScreen(MainScreen);
        }

        private void OnEnable()
        {
            // Main Screen
            StartGameButton.onClick.AddListener(() => StartCoroutine(LoadSceneRoutine("Demo")));
            OptionsButton.onClick.AddListener(() => OpenScreen(OptionsScreen));
            LoadManualSaveButton.onClick.AddListener(() => LoadManualSave());
            ExitButton.onClick.AddListener(() => Application.Quit());

            // Options Screen
            BackButton.onClick.AddListener(() => OpenScreen(MainScreen));
        }

        private void OnDisable()
        {
            // Main Screen
            StartGameButton.onClick.RemoveAllListeners();
            OptionsButton.onClick.RemoveAllListeners();
            LoadManualSaveButton.onClick.RemoveAllListeners();
            ExitButton.onClick.RemoveAllListeners();

            // Options Screen
            BackButton.onClick.RemoveAllListeners();
        }

        private IEnumerator LoadSceneRoutine(string sceneName)
        {
            OpenScreen(LoadingScreen);

            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);
            asyncOperation.allowSceneActivation = false;

            while (!asyncOperation.isDone)
            {
                LoadingFillImage.fillAmount = asyncOperation.progress;

                if (asyncOperation.progress >= 0.89f)
                {
                    asyncOperation.allowSceneActivation = true;
                    LoadingFillImage.fillAmount = 1f;

                    break;
                }

                yield return null;
            }
        }

        private void LoadManualSave()
        {
            LevelSaveData levelSaveData = SaveManager.GetLevelSaveData("ManualSave");
            SaveManager.LevelSaveData = levelSaveData;
            StartCoroutine(LoadSceneRoutine(levelSaveData.SceneName));
        }

        private void OpenScreen(GameObject screen)
        {
            for (int i = 0; i < _screens.Count; i++)
            {
                _screens[i].SetActive(false);
            }

            screen.SetActive(true);
        }
    }
}
