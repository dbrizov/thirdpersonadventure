﻿using UnityEngine;

namespace ThirdPersonAdventure
{
    public class VelocityMeasurer : MonoBehaviour
    {
        private Vector3[] _lastTwoPositions;
        private float _lastTwoPositionsDeltaTime;

        public Vector3 DeltaPosition => _lastTwoPositions[1] - _lastTwoPositions[0];
        public Vector3 Velocity => DeltaPosition / _lastTwoPositionsDeltaTime;
        public Vector3 HorizontalVelocity => new Vector3(Velocity.x, 0.0f, Velocity.z);
        public Vector3 VerticalVelocity => new Vector3(0.0f, Velocity.y, 0.0f);

        private void Awake()
        {
            _lastTwoPositions = new Vector3[2];
            _lastTwoPositions[0] = transform.position;
            _lastTwoPositions[1] = transform.position;
            _lastTwoPositionsDeltaTime = 1f;
        }

        public void Tick(float deltaTime)
        {
            if (deltaTime < float.Epsilon)
            {
                return;
            }

            _lastTwoPositions[0] = _lastTwoPositions[1];
            _lastTwoPositions[1] = transform.position;
            _lastTwoPositionsDeltaTime = deltaTime;
        }
    }
}
