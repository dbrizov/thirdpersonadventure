﻿using UnityEngine;

namespace ThirdPersonAdventure
{
    public class CharacterAudio : MonoBehaviour
    {
        [Header("Audio Sources")]
        public AudioSource JumpAudioSource;
        public AudioSource LandAudioSource;
        public AudioSource StepAudioSource;

        [Header("Audio Clips")]
        public AudioClip[] JumpAudioClips;
        public AudioClip[] LandAudioClips;
        public AudioClip[] StepAudioClips;

        public void PlayJumpSound()
        {
            JumpAudioSource.clip = JumpAudioClips.GetRandomElement();
            JumpAudioSource.Play();
        }

        public void PlayLandSound()
        {
            LandAudioSource.clip = LandAudioClips.GetRandomElement();
            LandAudioSource.Play();
        }

        public void PlayStepSound()
        {
            StepAudioSource.clip = StepAudioClips.GetRandomElement();
            StepAudioSource.Play();
        }
    }
}
