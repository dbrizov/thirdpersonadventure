﻿using UnityEngine;

namespace ThirdPersonAdventure
{
    class EnemyCharacterAnimator : MonoBehaviour
    {
        public string AttackAnimationName;

        private Animator _animator;
        private EnemyCharacter _character;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _character = GetComponentInParent<EnemyCharacter>();
        }

        public void UpdateState()
        {
            Vector3 horizontalVelocity = new Vector3(_character.Velocity.x, 0.0f, _character.Velocity.z);
            float normHorizontalSpeed = horizontalVelocity.magnitude / _character.MaxRunSpeed;
            _animator.SetFloat("HorizontalSpeed", normHorizontalSpeed);
        }

        public void PlayAttackAnimation()
        {
            _animator.SetTrigger("Attack");
        }

        public void PlayDeathAnimation()
        {
            _animator.SetTrigger("Death");
        }

        public bool IsAttackAnimationInProgress()
        {
            return _animator.IsAnimationInProgress(AttackAnimationName);
        }

        private void OnAttack()
        {
            _character.TryApplyDamageTo(_character.Target);
        }
    }
}
