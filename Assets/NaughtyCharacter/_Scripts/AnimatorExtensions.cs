﻿using UnityEngine;

namespace ThirdPersonAdventure
{
    public static class AnimatorExtensions
    {
        public static bool IsAnimationInProgress(this Animator animator, string animationName)
        {
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            bool isPlaying =
                stateInfo.IsName(animationName) &&
                stateInfo.normalizedTime < 1;

            return isPlaying;
        }
    }
}
