﻿using UnityEngine;

namespace ThirdPersonAdventure
{
    public class CharacterAnimator : MonoBehaviour
    {
        private static readonly int Param_HorizontalSpeed = Animator.StringToHash("HorizontalSpeed");
        private static readonly int Param_VerticalSpeed = Animator.StringToHash("VerticalSpeed");
        private static readonly int Param_IsGrounded = Animator.StringToHash("IsGrounded");
        private static readonly int Param_Attack = Animator.StringToHash("Attack");
        private static readonly int Param_AttackId = Animator.StringToHash("AttackId");

        public string AttackAnimationName;

        private Animator _animator;
        private Character _character;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _character = GetComponent<Character>();
        }

        public void UpdateState()
        {
            float normHorizontalSpeed = _character.HorizontalVelocity.magnitude / _character.MaxHorizontalSpeed;
            _animator.SetFloat(Param_HorizontalSpeed, normHorizontalSpeed);

            float jumpSpeed = _character.JumpSpeed;
            float normVerticalSpeed = Remap(_character.Velocity.y, -jumpSpeed, jumpSpeed, -1.0f, 1.0f);
            _animator.SetFloat(Param_VerticalSpeed, normVerticalSpeed);

            _animator.SetBool(Param_IsGrounded, _character.IsGrounded);
        }

        public void PlayAttackAnimation(int attackId)
        {
            _character.CanAttack = false;
            _animator.SetTrigger(Param_Attack);
            _animator.SetFloat(Param_AttackId, attackId);
        }

        public bool IsAttackAnimationInProgress()
        {
            return _animator.IsAnimationInProgress(AttackAnimationName);
        }

        private void OnAttackEnd()
        {
            _character.CanAttack = true;
        }

        private void OnResetAttackCombo()
        {
            _character.ResetAttackCombo();
        }

        private void OnApplyDamage()
        {
            _character.TryApplyDamage();
        }

        public static float Remap(float value, float fromMin, float fromMax, float toMin, float toMax)
        {
            float t = (value - fromMin) / (fromMax - fromMin);
            return Mathf.LerpUnclamped(toMin, toMax, t);
        }
    }
}