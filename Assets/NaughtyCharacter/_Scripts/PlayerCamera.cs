using UnityEngine;

namespace ThirdPersonAdventure
{
	public class PlayerCamera : MonoBehaviour
	{
		public Transform Rig; // The root transform of the camera rig
		public Transform Pivot; // The point at which the camera pivots around

		public void SetPosition(Vector3 position)
		{
			Rig.position = position;
		}

		public void SetControlRotation(Vector2 controlRotation)
		{
			// Y Rotation (Yaw Rotation)
			Quaternion rigTargetLocalRotation = Quaternion.Euler(0.0f, controlRotation.y, 0.0f);
			Rig.localRotation = rigTargetLocalRotation;

			// X Rotation (Pitch Rotation)
			Quaternion pivotTargetLocalRotation = Quaternion.Euler(controlRotation.x, 0.0f, 0.0f);
			Pivot.localRotation = pivotTargetLocalRotation;
		}
	}
}
