using UnityEngine;

namespace ThirdPersonAdventure
{
    public class PlayerInputComponent : MonoBehaviour
    {
        public Vector2 MoveInput { get; private set; }
        public Vector2 LastMoveInput { get; private set; }
        public bool HasMoveInput { get; private set; }
        public Vector2 CameraInput { get; private set; }
        public bool JumpInput { get; private set; }
        public bool AttackInput { get; private set; }

        public void UpdateInput()
        {
            if (Time.timeScale == 0)
            {
                return;
            }

            // Update move input (aka WASD)
            Vector2 moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            if (moveInput.sqrMagnitude > 1.0f)
            {
                moveInput.Normalize();
            }

            bool hasMoveInput = moveInput.sqrMagnitude > 0.0f;
            if (HasMoveInput && !hasMoveInput)
            {
                LastMoveInput = MoveInput;
            }

            MoveInput = moveInput;
            HasMoveInput = hasMoveInput;

            // Update camera input (mouse movement)
            CameraInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

            // Update jump input
            JumpInput = Input.GetButton("Jump");

            // Update attack input
            AttackInput = Input.GetButtonDown("Attack");
        }
    }
}
