﻿using UnityEngine;

namespace ThirdPersonAdventure
{
    public class World : MonoBehaviour
    {
        private static World _instance;

        [SerializeField]
        private Character _playerCharacter;

        private void Awake()
        {
            if (_instance != null)
            {
                GameObject.Destroy(this.gameObject);
                return;
            }

            _instance = this;
        }

        public static Character GetPlayerCharacter()
        {
            return _instance._playerCharacter;
        }
    }
}