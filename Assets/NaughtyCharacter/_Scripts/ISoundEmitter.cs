﻿using UnityEngine;

namespace ThirdPersonAdventure
{
    interface ISoundEmitter
    {
        float GetEmittedSound();
        Vector3 GetEmitterPosition();
    }
}
