﻿using UnityEngine;

namespace ThirdPersonAdventure
{
    public interface IDamageable
    {
        void ApplyDamage(int damage);
        Vector3 GetPosition();
    }
}
