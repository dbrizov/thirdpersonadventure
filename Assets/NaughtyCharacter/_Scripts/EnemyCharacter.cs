﻿using NaughtyAttributes;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace ThirdPersonAdventure
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class EnemyCharacter : SerializableMonoBehaviour, IDamageable, IHealthInterface
    {
        [Header("General Settings")]
        [ProgressBar("Health", nameof(MaxHealth), EColor.Red)]
        public int Health;
        public int MaxHealth = 100;

        [Header("Movement Settings")]
        public float MaxWalkSpeed = 1.5f;
        public float MaxRunSpeed = 6.0f;

        [Header("Vision Settings")]
        public Transform EyesPoint;
        public float FieldOfView = 170.0f;
        public float VisionRange = 25.0f;
        public LayerMask VisionLayerMask;

        [Header("Hearing Settings")]
        public float SoundThreshold = 7.9f;
        public float InnerHearingRange = 7.5f;
        public float OuterHearingRange = 15.0f;

        [Header("Attack Settings")]
        public int Damage = 10;
        public float AttackRange = 1.75f;
        public float AttackCooldown = 1.5f;

        [Header("Patrol Settings")]
        public Transform[] PatrolPoints;

        // Components
        private NavMeshAgent _agent;
        private EnemyCharacterAnimator _animator;

        // State
        private EnemyState _state;
        private bool _canSeePlayer;
        private Vector3? _loudPosition;

        private PatrolState _patrolState;
        private ChaseState _chaseState;
        private InvestigateState _investigateState;
        private AttackState _attackState;

        private List<ISoundEmitter> _externalSoundEmitters = new List<ISoundEmitter>();

        private bool _deathTriggered = false;

        public bool IsDead => Health == 0;
        public Vector3 Velocity => _agent.velocity;
        public IDamageable Target { get; private set; }


        private void Awake()
        {
            Health = MaxHealth;

            _agent = GetComponent<NavMeshAgent>();
            _animator = GetComponentInChildren<EnemyCharacterAnimator>();

            _patrolState = new PatrolState(this, _agent, PatrolPoints);
            _chaseState = new ChaseState(this, _agent);
            _investigateState = new InvestigateState(this, _agent);
            _attackState = new AttackState(this, _agent);
        }

        private void Start()
        {
            _externalSoundEmitters.Add(World.GetPlayerCharacter());
            ToState(_patrolState);
        }

        private void Update()
        {
            if (IsDead)
            {
                if (!_deathTriggered)
                {
                    Die();
                    _deathTriggered = true;
                }

                return;
            }

            _canSeePlayer = CanSeePlayer();
            CheckForLoudSounds();

            _state.Update(Time.deltaTime);
            //Debug.Log(_state.GetType().Name);

            _animator.UpdateState();
        }

        private void Die()
        {
            foreach (var collider in GetComponentsInChildren<Collider>())
            {
                collider.enabled = false;
            }
            _animator.PlayDeathAnimation();
        }

        private bool CanSeePlayer()
        {
            Vector3 myPosition = transform.position;
            myPosition.y = 0.0f;

            Character playerCharacter = World.GetPlayerCharacter();
            Vector3 playerPosition = playerCharacter.transform.position;
            playerPosition.y = 0.0f;

            Vector3 directionToPlayer = (playerPosition - myPosition).normalized;
            Vector3 forward = transform.forward;
            float cosTheta = Vector3.Dot(forward, directionToPlayer);
            float theta = Mathf.Acos(cosTheta) * Mathf.Rad2Deg;
            if (theta > FieldOfView / 2.0f)
            {
                return false;
            }

            Vector3 rayEnd = playerCharacter.VisionPoint.position;
            Vector3 rayStart = EyesPoint.position;
            Ray ray = new Ray(rayStart, (rayEnd - rayStart).normalized);
            RaycastHit hitInfo;

            if (Physics.Raycast(ray, out hitInfo, VisionRange, VisionLayerMask))
            {
                bool isPlayerVisible = (hitInfo.transform.gameObject.layer == playerCharacter.gameObject.layer);
                return isPlayerVisible;
            }
            else
            {
                return false;
            }
        }

        private void CheckForLoudSounds()
        {
            _loudPosition = null;
            foreach (var emitter in _externalSoundEmitters)
            {
                float distanceToEmitter = (emitter.GetEmitterPosition() - transform.position).magnitude;
                if (distanceToEmitter > OuterHearingRange)
                {
                    continue;
                }

                float outerRingRange = OuterHearingRange - InnerHearingRange;
                float outerRingPenetration = OuterHearingRange - distanceToEmitter;
                float emitterStrength = outerRingPenetration / outerRingRange; // in range [0, 1]
                float emittedSound = Mathf.Lerp(0.0f, emitter.GetEmittedSound(), emitterStrength);
                //Debug.Log(emittedSound);
                if (emittedSound > SoundThreshold)
                {
                    _loudPosition = emitter.GetEmitterPosition();
                    return;
                }
            }
        }

        private void ToState(EnemyState state)
        {
            _state?.Exit();
            _state = state;
            _state.Enter();
        }

        private bool HasReachedCurrentDestination()
        {
            bool destinationReached =
                !_agent.pathPending &&
                _agent.remainingDistance < _agent.stoppingDistance + 0.1f;

            return destinationReached;
        }

        private bool IsTargetInAttackRange(IDamageable target)
        {
            bool inAttackRange = (transform.position - target.GetPosition()).magnitude <= AttackRange;
            return inAttackRange;
        }

        private bool IsTargetInFront(IDamageable target)
        {
            Vector3 targetPos = target.GetPosition();
            targetPos.y = 0;

            Vector3 myPos = transform.position;
            myPos.y = 0;

            Vector3 directionToTarget = (targetPos - myPos).normalized;
            bool inFront = Vector3.Angle(transform.forward, directionToTarget) < 90;
            return inFront;
        }

        public bool TryApplyDamageTo(IDamageable damageable)
        {
            if (IsTargetInAttackRange(damageable) && IsTargetInFront(damageable))
            {
                damageable.ApplyDamage(Damage);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void FaceTargetPosition(Vector3 targetPosition)
        {
            Vector3 directionToTarget = targetPosition - transform.position;
            directionToTarget.y = 0;
            directionToTarget.Normalize();
            transform.rotation = Quaternion.LookRotation(directionToTarget, Vector3.up);
        }

        private void OnDrawGizmos()
        {
            DebugDrawVision();
            DebugDrawHearing();
        }

        private void DebugDrawVision()
        {
            Vector3 forward = this.transform.forward;
            Vector3 startDirection = Quaternion.Euler(0.0f, -FieldOfView / 2.0f, 0.0f) * forward;
            Vector3 endDirection = Quaternion.Euler(0.0f, FieldOfView / 2.0f, 0.0f) * forward;

            Gizmos.color = _canSeePlayer ? Color.red : Color.green;
            Gizmos.DrawLine(EyesPoint.position, EyesPoint.position + startDirection * VisionRange);
            Gizmos.DrawLine(EyesPoint.position, EyesPoint.position + endDirection * VisionRange);

            int steps = 16;
            float angleStep = FieldOfView / steps;
            for (int step = 0; step < steps; step++)
            {
                float startAngle = -FieldOfView / 2.0f + step * angleStep;
                float endAngle = -FieldOfView / 2.0f + (step + 1) * angleStep;
                Vector3 startDir = Quaternion.Euler(0.0f, startAngle, 0.0f) * forward;
                Vector3 endDir = Quaternion.Euler(0.0f, endAngle, 0.0f) * forward;
                Vector3 startPoint = EyesPoint.position + startDir * VisionRange;
                Vector3 endPoint = EyesPoint.position + endDir * VisionRange;

                Gizmos.DrawLine(startPoint, endPoint);
            }
        }

        private void DebugDrawHearing()
        {
            void DrawCircleXZ(Vector3 center, float range, Color color)
            {
                Gizmos.color = color;

                int steps = 16;
                float angleStep = (360.0f / steps) * Mathf.Deg2Rad;
                for (int step = 0; step < steps; step++)
                {
                    float startAngle = step * angleStep;
                    float endEngle = (step + 1) * angleStep;
                    Vector3 startPoint = center + new Vector3(range * Mathf.Cos(startAngle), 0.0f, range * Mathf.Sin(startAngle));
                    Vector3 endPoint = center + new Vector3(range * Mathf.Cos(endEngle), 0.0f, range * Mathf.Sin(endEngle));

                    Gizmos.DrawLine(startPoint, endPoint);
                }
            }

            Vector3 center = transform.position + Vector3.up * GetComponent<CapsuleCollider>().height / 2.0f;
            DrawCircleXZ(center, InnerHearingRange, Color.red);
            DrawCircleXZ(center, OuterHearingRange, Color.yellow);
        }

        public void ApplyDamage(int damage)
        {
            Health = Mathf.Max(0, Health - damage);
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public int GetHealth()
        {
            return Health;
        }

        public int GetMaxHealth()
        {
            return MaxHealth;
        }

        public override void ApplySaveData(ObjectSaveData data)
        {
            base.ApplySaveData(data);

            CharacterSaveData characterData = (CharacterSaveData)data;
            Health = characterData.Health;
            MaxHealth = characterData.MaxHealth;

            _agent.ResetPath();
            if (IsDead)
            {
                _state = null;                
            }
            else
            {
                _state = _patrolState;
            }
        }

        public override ObjectSaveData GetSaveData()
        {
            CharacterSaveData data = new CharacterSaveData(base.GetSaveData());
            data.Health = Health;
            data.MaxHealth = MaxHealth;

            return data;
        }

        private abstract class EnemyState
        {
            protected EnemyCharacter _character;
            protected NavMeshAgent _agent;

            public EnemyState(EnemyCharacter character, NavMeshAgent agent)
            {
                _character = character;
                _agent = agent;
            }

            public virtual void Enter() { }
            public virtual void Exit() { }
            public abstract void Update(float deltaTime);
        }

        private class PatrolState : EnemyState
        {
            private Transform[] _points;
            private int _destinationPointIndex;

            public PatrolState(EnemyCharacter character, NavMeshAgent agent, Transform[] points)
                : base(character, agent)
            {
                _agent = agent;
                _points = points;
                _destinationPointIndex = 0;
            }

            public override void Enter()
            {
                _agent.speed = _character.MaxWalkSpeed;
                _agent.ResetPath();
            }

            public override void Update(float deltaTime)
            {
                if (_character._canSeePlayer)
                {
                    _character.ToState(_character._chaseState);
                }
                else if (_character._loudPosition != null)
                {
                    _character._investigateState.PositionToInvestigate = _character._loudPosition.Value;
                    _character.ToState(_character._investigateState);
                }
                else
                {
                    if (_character.HasReachedCurrentDestination())
                    {
                        GoToNextPoint();
                    }
                }
            }

            private void GoToNextPoint()
            {
                _agent.destination = _points[_destinationPointIndex].position;
                _destinationPointIndex = (_destinationPointIndex + 1) % _points.Length;
            }
        }

        private class ChaseState : EnemyState
        {
            private Vector3 _lastKnownPlayerPosition;
            private float _forgetDuration;

            public ChaseState(EnemyCharacter character, NavMeshAgent agent)
               : base(character, agent)
            {
            }

            public override void Enter()
            {
                _forgetDuration = 5.0f;
                _agent.speed = _character.MaxRunSpeed;
                _agent.ResetPath();
            }

            public override void Update(float deltaTime)
            {
                if (_character._canSeePlayer)
                {
                    Character playerCharacter = World.GetPlayerCharacter();
                    if (_character.IsTargetInAttackRange(playerCharacter))
                    {
                        _character.ToState(_character._attackState);
                    }

                    _lastKnownPlayerPosition = playerCharacter.transform.position;
                }
                else if (_character.HasReachedCurrentDestination())
                {
                    _forgetDuration -= deltaTime;
                    if (_forgetDuration < 0.0f)
                    {
                        _character.ToState(_character._patrolState);
                    }
                    else if (_character._loudPosition != null)
                    {
                        _character._investigateState.PositionToInvestigate = _character._loudPosition.Value;
                        _character.ToState(_character._investigateState);
                    }
                }

                _agent.destination = _lastKnownPlayerPosition;
            }
        }

        private class InvestigateState : EnemyState
        {
            public Vector3 PositionToInvestigate { get; set; }

            private float _forgetDuration;

            public InvestigateState(EnemyCharacter character, NavMeshAgent agent)
               : base(character, agent)
            {
            }

            public override void Enter()
            {
                _forgetDuration = 5.0f;
                _agent.speed = _character.MaxWalkSpeed;
                _agent.ResetPath();
                _agent.destination = PositionToInvestigate;
            }

            public override void Update(float deltaTime)
            {
                if (_character._canSeePlayer)
                {
                    _character.ToState(_character._chaseState);
                }
                else if (_character._loudPosition != null)
                {
                    _character._investigateState.PositionToInvestigate = _character._loudPosition.Value;
                    _character.ToState(_character._investigateState);
                }
                else
                {
                    if (_character.HasReachedCurrentDestination())
                    {
                        _forgetDuration -= deltaTime;
                        if (_forgetDuration < 0.0f)
                        {
                            _character.ToState(_character._patrolState);
                        }
                    }
                }
            }
        }

        private class AttackState : EnemyState
        {
            private float _attackCooldown;

            public AttackState(EnemyCharacter character, NavMeshAgent agent)
               : base(character, agent)
            {
            }

            public override void Enter()
            {
                _attackCooldown = 0.0f;
                _character.Target = World.GetPlayerCharacter();
            }

            public override void Update(float deltaTime)
            {
                _attackCooldown -= deltaTime;

                if (_character.IsTargetInAttackRange(_character.Target))
                {
                    _character.FaceTargetPosition(_character.Target.GetPosition());
                    if (_attackCooldown <= 0)
                    {
                        _character._animator.PlayAttackAnimation();
                        _attackCooldown = _character.AttackCooldown;
                    }
                }
                else
                {
                    bool isAttackInProgress = _character._animator.IsAttackAnimationInProgress();
                    if (isAttackInProgress)
                    {
                        return;
                    }

                    if (_character._canSeePlayer)
                    {
                        _character.ToState(_character._chaseState);
                    }
                    else if (_character._loudPosition != null)
                    {
                        _character._investigateState.PositionToInvestigate = _character._loudPosition.Value;
                        _character.ToState(_character._investigateState);
                    }
                    else
                    {
                        _character.ToState(_character._patrolState);
                    }
                }
            }
        }
    }
}