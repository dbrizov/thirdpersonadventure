using UnityEngine;
using NaughtyAttributes;
using System.Collections.Generic;

namespace ThirdPersonAdventure
{
    public class Character : SerializableMonoBehaviour, ISoundEmitter, IDamageable
    {
        [Header("Components")]
        public CharacterController CharacterController;
        public CharacterAnimator CharacterAnimator;
        public VelocityMeasurer VelocityMeasurer;
        public PlayerInputComponent PlayerInput;
        public PlayerCamera PlayerCamera;

        [Header("General Settings")]
        [ProgressBar("Health", nameof(MaxHealth), EColor.Red)]
        public int Health = 100;
        public int MaxHealth = 100;
        public Transform VisionPoint;

        [Header("Camera Settings")]
        public float MinPitchAngle = -45.0f;
        public float MaxPitchAngle = 75.0f;
        public float ControlRotationSensitivity = 1.0f;

        [Header("Movement Settings")]
        public float Acceleration = 25.0f;
        public float Decceleration = 25.0f;
        public float MaxHorizontalSpeed = 8.0f;
        public float JumpSpeed = 10.0f;

        [Header("Gravity Settings")]
        public float Gravity = 20.0f;
        public float MaxFallSpeed = 40.0f;

        [Header("Rotation Settings")]
        public float RotationSpeed = 720.0f;

        [Header("Ground Settings")]
        public LayerMask GroundLayers;
        public float GroundCheckSphereCastRadius = 0.35f;
        public float GroundCheckSphereCastDistance = 0.15f;

        [Header("Attack Settings")]
        public int Damage = 50;
        public float AttackRange = 2;
        public float AttackAngle = 90;
        public LayerMask EnemyLayers;
        public GameObject Weapon;

        public bool CanAttack { get; set; } = true;
        public bool IsGrounded { get; private set; }

        public Vector3 Velocity => VelocityMeasurer.Velocity;
        public Vector3 HorizontalVelocity => VelocityMeasurer.HorizontalVelocity;
        public Vector3 VerticalVelocity => VelocityMeasurer.VerticalVelocity;

        private Vector2 _controlRotation; // X (Pitch), Y (Yaw)
        private float _verticalSpeed;
        private float _horizontalSpeed;
        private float _targetHorizontalSpeed;

        private int _currentAttackIndex = -1;
        private int[] _attackCombo = new int[] { 0, 1, 2, 3 };

        private void Start()
        {
            Health = MaxHealth;
            HideWeapon();
        }

        private void Update()
        {
            PlayerInput.UpdateInput();

            UpdateControlRotation();
            UpdateHorizontalSpeed(Time.deltaTime);
            UpdateVerticalSpeed(Time.deltaTime);

            Vector3 horizontalVelocity = GetMovementDirection() * _horizontalSpeed;
            Vector3 verticalVelocity = Vector3.up * _verticalSpeed;
            Vector3 velocity = horizontalVelocity + verticalVelocity;
            CharacterController.Move(velocity * Time.deltaTime);
            VelocityMeasurer.Tick(Time.deltaTime);

            OrientRotationToMovement(horizontalVelocity, Time.deltaTime);

            IsGrounded = CheckGrounded();

            if (CanAttack && PlayerInput.AttackInput)
            {
                Attack();
            }

            CharacterAnimator.UpdateState();

            PlayerCamera.SetPosition(transform.position);
            PlayerCamera.SetControlRotation(_controlRotation);
        }

        public void Attack()
        {
            transform.rotation = Quaternion.LookRotation(GetMovementDirection(), Vector3.up);
            _currentAttackIndex = (_currentAttackIndex + 1) % _attackCombo.Length;
            CharacterAnimator.PlayAttackAnimation(_attackCombo[_currentAttackIndex]);
        }

        public void ResetAttackCombo()
        {
            _currentAttackIndex = -1;
        }

        public void ShowWeapon()
        {
            Weapon.SetActive(true);
        }

        public void HideWeapon()
        {
            Weapon.SetActive(false);
        }

        public bool TryApplyDamage()
        {
            bool success = false;
            HashSet<IDamageable> alreadyHitTargets = new HashSet<IDamageable>();

            int steps = 5;
            float angleStep = AttackAngle / steps;
            for (int step = 0; step <= steps; step++)
            {
                float angle = -AttackAngle / 2.0f + step * angleStep;
                Vector3 rayDirection = Quaternion.Euler(0.0f, angle, 0.0f) * transform.forward;
                Vector3 rayStart = transform.position + Vector3.up * CharacterController.height / 2.0f;

                Ray ray = new Ray(rayStart, rayDirection);
                if (Physics.SphereCast(ray, 0.25f, out RaycastHit hit, AttackRange, EnemyLayers))
                {
                    IDamageable hitTarget = hit.transform.GetComponent<IDamageable>();
                    if (hitTarget != null && !alreadyHitTargets.Contains(hitTarget))
                    {
                        hitTarget.ApplyDamage(Damage);
                        alreadyHitTargets.Add(hitTarget);
                        success = true;
                    }
                }

                Debug.DrawLine(rayStart, rayStart + rayDirection * AttackRange, Color.white, duration: 2.0f);
            }

            return success;
        }

        private Vector3 GetMovementDirection()
        {
            // Calculate the move direction relative to the character's yaw rotation
            Quaternion yawRotation = Quaternion.Euler(0.0f, _controlRotation.y, 0.0f);
            Vector3 forward = yawRotation * Vector3.forward;
            Vector3 right = yawRotation * Vector3.right;
            Vector2 moveInput = PlayerInput.HasMoveInput ? PlayerInput.MoveInput : PlayerInput.LastMoveInput;
            Vector3 movementDirection = (forward * moveInput.y + right * moveInput.x);

            if (movementDirection.sqrMagnitude > 1f)
            {
                movementDirection.Normalize();
            }

            return movementDirection;
        }

        private void UpdateControlRotation()
        {
            Vector2 camInput = PlayerInput.CameraInput;

            // Adjust the pitch angle (X Rotation)
            float pitchAngle = _controlRotation.x;
            pitchAngle -= camInput.y * ControlRotationSensitivity;
            pitchAngle %= 360.0f;
            pitchAngle = Mathf.Clamp(pitchAngle, MinPitchAngle, MaxPitchAngle);

            // Adjust the yaw angle (Y Rotation)
            float yawAngle = _controlRotation.y;
            yawAngle += camInput.x * ControlRotationSensitivity;
            yawAngle %= 360.0f;

            _controlRotation = new Vector2(pitchAngle, yawAngle);
        }

        private bool CheckGrounded()
        {
            Vector3 spherePosition = transform.position;
            spherePosition.y = transform.position.y + GroundCheckSphereCastRadius - GroundCheckSphereCastDistance;
            bool isGrounded = Physics.CheckSphere(spherePosition, GroundCheckSphereCastRadius, GroundLayers, QueryTriggerInteraction.Ignore);

            return isGrounded;
        }

        private void UpdateHorizontalSpeed(float deltaTime)
        {
            if (IsGrounded && CharacterAnimator.IsAttackAnimationInProgress())
            {
                _horizontalSpeed = 0;
            }
            else
            {
                _targetHorizontalSpeed = PlayerInput.MoveInput.magnitude * MaxHorizontalSpeed;
                float acceleration = PlayerInput.HasMoveInput ? Acceleration : Decceleration;

                _horizontalSpeed = Mathf.MoveTowards(_horizontalSpeed, _targetHorizontalSpeed, acceleration * deltaTime);
            }
        }

        private void UpdateVerticalSpeed(float deltaTime)
        {
            if (IsGrounded)
            {
                if (PlayerInput.JumpInput)
                {
                    _verticalSpeed = JumpSpeed;
                }
                else
                {
                    _verticalSpeed = -5.0f;
                }
            }
            else
            {
                _verticalSpeed = Mathf.MoveTowards(_verticalSpeed, -MaxFallSpeed, Gravity * deltaTime);
            }
        }

        private void OrientRotationToMovement(Vector3 horizontalVelocity, float deltaTime)
        {
            if (horizontalVelocity.magnitude > 0.0f)
            {
                Quaternion targetRotation = Quaternion.LookRotation(horizontalVelocity, Vector3.up);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, RotationSpeed * deltaTime);
            }
        }

        public float GetEmittedSound()
        {
            return HorizontalVelocity.magnitude;
        }

        public Vector3 GetEmitterPosition()
        {
            return transform.position;
        }

        public void ApplyDamage(int damage)
        {
            Health = Mathf.Max(0, Health - damage);
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public override void ApplySaveData(ObjectSaveData data)
        {
            base.ApplySaveData(data);

            CharacterSaveData characterData = (CharacterSaveData)data;
            Health = characterData.Health;
            MaxHealth = characterData.MaxHealth;
        }

        public override ObjectSaveData GetSaveData()
        {
            CharacterSaveData data = new CharacterSaveData(base.GetSaveData());
            data.Health = Health;
            data.MaxHealth = MaxHealth;

            return data;
        }
    }
}
